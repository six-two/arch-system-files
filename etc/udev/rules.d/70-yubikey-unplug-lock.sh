#!/usr/bin/env bash
# @TODO: Can I put it in the same directory as the rules file, or does it need to be somewhere else (like /usr/bin/)?

KEY_NAME="Yubikey 5"
DISABLE_FILE="/tmp/lock-on-unplug.disabled"
NOTIFY_ON_STATUS_CHANGE=true
APP_NAME="Lock on unplug"

function action_lock {
  if [ -f "${DISABLE_FILE}" ]; then
    notify-send "${APP_NAME}" "You unplugged your '${KEY_NAME}' key, but locking the screen on unplug was disabled."
  else
    if [ $(id -u) = 0 ]; then
      # As root: lock all sessions
      loginctl lock-sessions
    else
      # As normal user: lock current session
      loginctl lock-session
    fi
  fi
}

function action_enable {
  rm -f "${DISABLE_FILE}"

  if [ "${NOTIFY_ON_STATUS_CHANGE}" = true ] ; then
    notify-send "Enabled ${APP_NAME}" "If you unplug '${KEY_NAME}' your screen will be locked"
  fi
}

function action_disable {
  touch "${DISABLE_FILE}"

  if [ "${NOTIFY_ON_STATUS_CHANGE}" = true ] ; then
    notify-send "Disabled ${APP_NAME}" "You can now unplug '${KEY_NAME}' without locking the screen"
  fi
}

function show_help {
  echo "Usage: <action>"
  echo
  echo "Possible actions:"
  echo " - lock: Lock the screen if enabled, otherwise show a notification"
  echo " - status: Show whether screen locking is enabled"
  echo " - enable / on: Enable screen locking"
  echo " - disable / off: Disable screen locking"
  echo " - toggle: Toggle between enabled and disabled"
}

# for debuggings
echo "[$(date)] $(whoami): Called with $# argument(s): '$@'" >> /tmp/yubikey-unplug-log.sh

case "$1" in
  lock)
    action_lock
    ;;

  status)
    if [ -f "${DISABLE_FILE}" ]; then
      echo "disabled"
    else
      echo "enabled"
    fi
    ;;

  toggle)
    if [ -f "${DISABLE_FILE}" ]; then
      action_enable
    else
      action_disable
    fi
    ;;

  enable | on)
    action_enable
    ;;

  disable | off)
    action_disable
    ;;

  *)
    show_help
    exit 1
    ;;
esac