#!/usr/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "[*] This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

# You can sustomize this, see https://wiki.qemu.org/Documentation/9psetup#Performance_Considerations_(msize)
MSIZE=102400

# Switch to current directory
cd -- "$( dirname -- "${BASH_SOURCE[0]}" )"

# Read the list of tags from qemu-guest-9p-mount.txt
for TAG in $(cat ./qemu-guest-9p-mount.txt); do
    # @TODO: Validate / sanitize TAG?
    echo "[*] Trying to mount tag '$TAG'"
    if [[ ! -d "/media/$TAG/" ]]; then
        echo "[*] Creating missing directory /media/$TAG/"
        mkdir -p "/media/$TAG/"
    fi
    if mount -t 9p -o trans=virtio "$TAG" "/media/$TAG/" "-oversion=9p2000.L,msize=${MSIZE}"; then
        echo "[+] Mounted to /media/$TAG/"
    fi
done
