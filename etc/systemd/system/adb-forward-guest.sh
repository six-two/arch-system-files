#!/usr/bin/bash

is_installed() {
    command -v "$1" &>/dev/null
}

# The default adb port
PORT=5037

# In case real adb is running already, kill it first
# But if you already have manual forwarding set up (for example with socat), 
# do NOT send the kill message, since it would kill adb on the host/remote computer
if is_installed lsof; then
    LSOF_OUTPUT="$(lsof -i:"$PORT")"
    if [[ $? -eq 0 ]]; then
        echo "[*] Found service listening on port:"
        echo "$LSOF_OUTPUT"
        if echo "$LSOF_OUTPUT" | grep "^adb\s"; then
            echo "[*] Service seems to be adb, sending kill message"
            adb kill-server
        fi
    fi
else
    echo "[-] lsof is not installed. Please install it, since it is required to check if adb is running (and should be killed)"
fi

while true; do
    HOST_IP="$(ip -4 route show default | cut -d ' ' -f3)"
    [[ -n HOST_IP ]] && break
    # delay and try again later
    sleep 1
done

echo "[*] Host IP (default gateway) seems to be $HOST_IP"
socat "TCP-LISTEN:${PORT},reuseaddr,fork" "TCP-CONNECT:${HOST_IP}:${PORT}"
