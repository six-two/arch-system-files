#!/usr/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "[*] This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

# In case it is running, stop the real usbmuxd
if systemctl is-active --quiet usbmuxd; then
    echo "[*] usbmuxd is already running, stopping it now"
    systemctl stop usbmuxd
fi

while true; do
    HOST_IP="$(ip -4 route show default | cut -d ' ' -f3)"
    [[ -n HOST_IP ]] && break
    # delay and try again later
    sleep 1
done

echo "[*] Host IP (default gateway) seems to be $HOST_IP"
socat UNIX-LISTEN:/var/run/usbmuxd,mode=666,reuseaddr,fork "TCP-CONNECT:${HOST_IP}:5550"
