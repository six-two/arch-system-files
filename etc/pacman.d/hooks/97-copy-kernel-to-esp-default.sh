#!/usr/bin/bash
# I chose that name since it needs to come after the following hooks:
# /usr/share/libalpm/hooks/90-mkinitcpio-install.hook
# /usr/share/libalpm/hooks/95-sbupdate.hook

if [[ $EUID -ne 0 ]]; then
  echo "[*] This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

if [[ ! -d "/efi" ]]; then
    echo "[!] /efi/ directory not found. Please create it and mount your ESP there"
    exit 1
fi

OUT=/efi/EFI/Boot/bootx64.efi
CONFIG_FILE=/etc/pacman.d/hooks/97-copy-kernel-to-esp-default.txt

# Check files listed in config file, copy the first one to the default ESP location
while read -r line; do
    echo "[*] Testing if $line exists"
    if [[ -n "$line" && -f "$line" ]]; then
        # Ensure that the output directory exists
        mkdir -p /efi/EFI/Boot/
        # If it exists, back up the previous file
        if [[ -f "${OUT}" ]]; then
            echo "[*] Backing up $OUT"
            if [[ -f "${OUT}.bak" ]]; then
                rm "${OUT}.bak"
            fi
            mv "$OUT" "${OUT}.bak"
        fi
        # Now copy the file and exit
        echo "[*] Copying $line to $OUT"
        cp "$line" "$OUT"
        exit 0
    fi
done < "${CONFIG_FILE}"
# cp /efi/EFI/Linux/linux-lts-signed.efi /efi/EFI/Boot/bootx64.efi
echo "[!] No file found to copy. Please list it in ${CONFIG_FILE}"

