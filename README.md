# Arch Linux System Files

Contains stuff like:

- custom systemd services
- custom pacman hooks

## etc/pacman.d/hooks/

- `97-copy-kernel-to-esp-default.*`: Copy a custom file to /efi/EFI/Boot/bootx64.efi.
    This (/EFI/Boot/bootx64.efi on EFI System Partition) is the location UEFI computers search for the default boot entry for a disk.
- `nmap-setcap-network.hook`:
    Set capabilities for nmap, so that it can use raw network sockets.
    Afterwards you can then call nmap without root, but you need to set the `NMAP_PRIVILEGED` variable to `yes`.
    Example:
    ```bash
    NMAP_PRIVILEGED=yes nmap -sS localhost
    ```
    You will need to reinstall nmap to trigger this hook initially:
    ```bash
    sudo pacman -S nmap
    ```
- `sign-fwupd-secureboot.hook`: Sign fwupd for systems with secure boot.
    You also need to add the following to `/etc/fwupd/fwupd.conf`:
    ```conf
    [uefi_capsule]
    DisableShimForSecureBoot=true
    ```
    For more info see the [ArchWiki](https://wiki.archlinux.org/title/Fwupd#Using_your_own_keys)

## etc/systemd/system/

- `adb-forward-.*`: Forward adb socket from VM to host.
    - `adb-forward-guest.*`: Enable this in the VM.
    - `adb-forward-host.*`: Start adb listening on all interfaces. Enable this on the host. **CONFIGURE YOUR FIREWALL TO ONLY ALLOW TRUSTED SYSTEMS TO CONNECT TO PORT 5037/TCP**, since anyone with access to this port can run arbitrary commands on your phone.
- `qemu-guest-9p-mount.*`: Mount shared folders in QEMU vm.
    Overwrite the default tag list located at `/etc/systemd/system/qemu-guest-9p-mount.txt` with the tags you want to mount.
    Then enable the timer (not the service):
    ```bash
    sudo systemctl enable qemu-guest-9p-mount.timer
    ```
- `usbmuxd-forward-*`: Used for forwarding usbmuxd (used by `idevice*` tools to communicate to an iPhone via USB) from the host to a VM.
    - `usbmuxd-forward-guest.*`: Enable this in the VM.
    - `usbmuxd-forward-host.*`: Enable this on the host.

## etc/udev/rules.d

- `70-yubikey-unplug-lock.*`: Should lock the scrren if you unplug your Yubikey. @TODO: For some reason it does not currently work for me.
    Useful command to test what device ID (and other attributes) your Yubikey has:
    ```bash
    udevadm monitor --environment --udev
    ```

## efi/loader

Basic `systemd-boot` config files for default kernel and intel or AMD CPUs.
Your root partition should have the label `LinuxRoot`.
This way of determining the disk has the following advantages and disadvantages:

- Good: Does not depend on order of disks (unlike `/dev/sda2`).
- Good: Does not depend on the type of controller (virtio: `/dev/vda2`, SATA: `/dev/sda2`, NVMe: `/dev/nvme0n1p2`).
- Good: Does not need to be changed for each system (unlike `/dev/disk/by-uuid/123...`).
- Bad: If you have/want multiple OSs (for dual booting), you need to modify it.

If you want a different way of addressing, just replace `/dev/disk/by-label/LinuxRoot` with the path to your disk.

### Setup

If you use LUKS, you can set the label like this (replace `/dev/sda2` with the partition of the container):
```bash
sudo cryptsetup config /dev/sda2 --label LinuxRoot
```
If it is unencrypted you could probably set the label with `gparted`.
