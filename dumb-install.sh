#!/usr/bin/bash
if [[ $EUID -ne 0 ]]; then
  echo "[*] This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

echo "[!] Temporary installer, will overwrite any manual changes and may not be easy to remove"

# Switch to current directory
cd -- "$( dirname -- "${BASH_SOURCE[0]}" )"

install_all_files() {
    echo "[*] Installing files $1/*"
    [[ ! -d "/$1" ]] && mkdir -p "/$1"
    cp "$1"/* "/$1"
}

install_all_files etc/systemd/system
install_all_files etc/pacman.d/hooks
install_all_files etc/udev/rules.d
install_all_files boot

# Reload config files, so that services can immediately be (re)started manuelly afterwards
echo "[*] Reloading systemd files"
sudo systemctl daemon-reload

# Reload udev canfig files, so that the changes immediately are active
echo "[*] Reloading udev rules"
sudo udevadm control --reload
